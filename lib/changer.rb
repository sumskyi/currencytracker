# a bit modified http://www.dotnetperls.com/recursion-ruby way
class Changer
  def initialize(total, amounts, tolerance)
    @results = []
    @total, @amounts = total, amounts
    @tolerance = tolerance
  end

  def run
    change([], @amounts, 0, 0, @total)
    @results
  end

  private

  def change(coins, amounts, highest, sum, goal)
    # Return if we have too much money.
    return if sum > goal

    # Display result if we have correct change.
    if sum >= goal - @tolerance
      display(coins, amounts)
    end

    # Loop over coin amounts and try adding them.
    amounts.each do |value|
      if value >= highest
        # Copy the coins array, add value to it.
        copy = Array[]
        copy.concat coins
        copy.push(value)
        # Recursive call: add further coins if needed.
        change(copy, amounts, value, sum + value, goal)
      end
    end
  end

  def display(coins, amounts)
    # Display all the coins.
    @results << amounts.each_with_object([]) do |amount, memo|
      coins.each do |coin|
        if coin == amount
          memo << amount
        end
      end
    end
  end
end
