require 'rails_helper'

RSpec.describe 'Authentication', type: :request do
  let!(:user) { Fabricate(:user) }
  let(:params) do
    { email: user.email,
      password: '666666' }
  end

  describe "POST /api/token", reqres_title: 'Create authentication token' do
    it 'responds with authentication token' do
      post '/api/token.json', params
      expect(response).to have_http_status(200)
    end
  end
end
