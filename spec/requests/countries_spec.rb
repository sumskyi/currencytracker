require 'rails_helper'

RSpec.describe "Countries", type: :request do
  include_context 'login via authentication token'

  let!(:usa) { Fabricate(:usa) }
  let!(:canada) { Fabricate(:canada) }
  let!(:ukraine) { Fabricate(:ukraine) }

  let!(:visit) { Fabricate(:visit, user: user, country: ukraine) }

  describe "/api/countries" do
    let(:parsed_ukraine) do
      parsed_response.find{|country| country['name'] == 'Ukraine'}
    end

    let(:parsed_canada) do
      parsed_response.find{|country| country['name'] == 'Canada'}
    end

    context 'pagination' do
      context 'no page provided' do
        let(:params) { {} }
        it 'shows the full list of countries' do
          get '/api/countries.json', params, authorization
          expect(parsed_ukraine).to be
          expect(parsed_ukraine['visited']).to eq true
          expect(parsed_canada['visited']).to eq false
        end
      end

      context '1st page' do
        let(:params) { { page: 1 } }
        it 'shows the 1st page of countries' do
          get '/api/countries.json', params, authorization
          expect(parsed_ukraine).to be
          expect(parsed_ukraine['visited']).to eq true
          expect(parsed_canada['visited']).to eq false
        end
      end

      context 'non-existent page', :skip_reqres do
        let(:params) { { page: 1001 } }
        it 'shows the empty list' do
          get '/api/countries.json', params, authorization
          expect(parsed_response.size).to eq 0
        end
      end
    end
  end

  describe "/api/countries/:id" do
    it 'shows country' do
      get "/api/countries/#{usa.id}.json", nil, authorization
      expect(parsed_response['name']).to eq 'United States'
      expect(parsed_response['visited']).to eq false
    end
  end

  describe "/api/countries/:id/visit" do
    it 'set country as visited' do
      expect {
        post "/api/countries/#{usa.id}/visit.json", nil, authorization
      }.to change(Visit, :count).by(1)
    end
  end

  describe "/api/countries/visited" do
    let(:visited) { parsed_response['visited'] }
    let(:unvisited) { parsed_response['unvisited'] }

    it 'shows visited vs unvisited' do
      get "/api/countries/visited.json", nil, authorization
      expect(visited.size).to eq 1
      expect(unvisited.size).to eq 2
    end
  end

  describe "/api/countries/visits_over_time" do
    let(:visited) { parsed_response['visits'] }

    context 'no date params provided' do
      let(:params) do
        {}
      end

      it 'shows visited over time' do
        get "/api/countries/visits_over_time.json", params, authorization
        expect(visited.size).to eq 1
      end
    end

    context 'old date params provided', :skip_reqres do
      let(:params) do
        { start_date: '2014-01-01',
          end_date: '2014-12-31' }
      end

      it 'shows visited over time' do
        get "/api/countries/visits_over_time.json", params, authorization
        expect(visited.size).to eq 0
      end
    end

    context 'valid date params provided' do
      let(:now_year) { Date.today.year }
      let(:params) do
        { start_date: format('%{year}-01-01', year: now_year),
          end_date: format('%{year}-12-31', year: now_year) }
      end

      it 'shows visited over time' do
        get "/api/countries/visits_over_time.json", params, authorization
        expect(visited.size).to eq 1
      end
    end
  end

  describe '/api/countries/unvisited' do
    let!(:usd) { Fabricate(:usd, country: usa) }
    let!(:uah) { Fabricate(:uah, country: ukraine) }
    let!(:cad) { Fabricate(:cad, country: canada) }

    let(:params) do
      { capacity: 27 }
    end
    let(:parsed_cad) { parsed_response['unvisited'].find{|el| el['code'] == 'cad'} }
    let(:parsed_usd) { parsed_response['unvisited'].find{|el| el['code'] == 'usd'} }

    it 'shows visited vs unvisited' do
      get "/api/countries/unvisited.json", params, authorization
      expect(parsed_cad['number']).to eq 3
      expect(parsed_usd['number']).to eq 4
    end
  end

end
