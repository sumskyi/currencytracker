require 'rails_helper'

RSpec.describe 'Currencies', type: :request do
  include_context 'login via authentication token'

  let(:ukraine) { Fabricate(:ukraine) }
  let(:canada)  { Fabricate(:canada) }

  let!(:uah)   { Fabricate(:uah, country: ukraine) }
  let!(:usd)   { Fabricate(:usd, country: canada) }
  let!(:visit) { Fabricate(:visit, user: user, country: ukraine) }

  describe '/api/currencies' do
    context 'pagination' do
      context 'no page provided' do
        let(:params) { {} }
        it 'shows the list of currencies' do
          get '/api/currencies.json', params, authorization
          expect(parsed_response[0]['name']).to eq 'Ukrainian hryvnya'
          expect(parsed_response.size).to eq 2
        end
      end

      context '1st page' do
        let(:params) { { page: 1 } }
        it 'shows the list of currencies' do
          get '/api/currencies.json', params, authorization
          expect(parsed_response[0]['name']).to eq 'Ukrainian hryvnya'
          expect(parsed_response.size).to eq 2
        end
      end

      context 'non-existent page', :skip_reqres do
        let(:params) { { page: 1001 } }
        it 'shows the empty list' do
          get '/api/currencies.json', params, authorization
          expect(parsed_response.size).to eq 0
        end
      end
    end
  end

  describe '/api/currencies/:id' do
    it 'shows currency' do
      get "/api/currencies/#{uah.id}.json", nil, authorization
      expect(parsed_response['name']).to eq 'Ukrainian hryvnya'
      expect(parsed_response['collected']).to eq true
    end
  end

  describe '/api/currencies/collected' do
    let(:collected) { parsed_response['collected'] }
    let(:uncollected) { parsed_response['uncollected'] }

    it 'shows collected vs uncollected' do
      get '/api/currencies/collected.json', nil, authorization
      expect(collected.size).to eq 1
      expect(uncollected.size).to eq 1
    end
  end

  describe "/api/currencies/collected_over_time" do
    let(:collected) { parsed_response['collected'] }

    context 'no date params provided' do
      let(:params) do
        {}
      end

      it 'shows collected over time' do
        get "/api/currencies/collected_over_time.json", params, authorization
        expect(collected.size).to eq 1
      end
    end

    context 'old date params provided', :skip_reqres do
      let(:params) do
        { start_date: '2014-01-01',
          end_date: '2014-12-31' }
      end

      it 'shows collected over time' do
        get "/api/currencies/collected_over_time.json", params, authorization
        expect(collected.size).to eq 0
      end
    end

    context 'valid date params provided' do
      let(:now_year) { Date.today.year }
      let(:params) do
        { start_date: format('%{year}-01-01', year: now_year),
          end_date: format('%{year}-12-31', year: now_year) }
      end

      it 'shows collected over time' do
        get "/api/currencies/collected_over_time.json", params, authorization
        expect(collected.size).to eq 1
      end
    end
  end
end
