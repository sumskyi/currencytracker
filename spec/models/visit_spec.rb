require 'rails_helper'

RSpec.describe Visit, type: :model do
  it { should belong_to(:user) }
  it { should belong_to(:country) }

  describe '#by_user_and_date' do
    let(:user)    { Fabricate(:user) }
    let(:ukraine) { Fabricate(:ukraine) }
    let(:usa)     { Fabricate(:usa) }
    let!(:visit1) { Fabricate(:visit, user: user, country: ukraine) }
    let!(:visit2) { Fabricate(:visit, user: user, country: usa, created_at: 5.years.ago) }

    context 'no date provided' do
      subject { Visit.by_user_and_date(user).to_a }
      it 'returns all records' do
        expect(subject.size).to eq 2
      end
    end

    context 'old date range' do
      let(:very_long_time_ago) do
        { start_date: 101.years.ago.to_s(:date_db),
          end_date: 100.years.ago.to_s(:date_db),
        }
      end
      subject { Visit.by_user_and_date(user, very_long_time_ago).to_a }
      it 'returns no records' do
        expect(subject.size).to eq 0
      end
    end

    context 'current date range' do
      let(:current_year) { Date.today.year }
      let(:current_year_range) do
        { start_date: format('%{year}-01-01', year: current_year),
          end_date: format('%{year}-12-31', year: current_year) }
      end

      subject { Visit.by_user_and_date(user, current_year_range).to_a }
      it 'returns current year records' do
        expect(subject.size).to eq 1
      end
    end
  end
end
