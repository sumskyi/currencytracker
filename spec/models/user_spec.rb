require 'rails_helper'

RSpec.describe User, type: :model do
  it { should validate_length_of(:password).is_at_least(3)  }
  it { should validate_presence_of(:password_confirmation) }
  it { should validate_confirmation_of(:password) }
  it { should validate_uniqueness_of(:email) }

  it { should have_many(:visits) }
  it { should have_many(:countries) }

  describe '#authentication_token' do
    let(:user) { Fabricate(:user) }

    it 'sets authentication_token' do
      user.set_authentication_token
      expect(user.authentication_token).to be
    end
  end
end
