require 'rails_helper'

RSpec.describe Country, type: :model do
  it { should validate_presence_of(:name) }
  it { should validate_presence_of(:code) }

  it { should have_one(:currency) }
  it { should have_many(:visits) }
  it { should have_many(:users) }

end
