require 'rails_helper'

RSpec.describe Currency, type: :model do
  it { should validate_presence_of(:name) }
  it { should validate_presence_of(:code) }
  it { should validate_presence_of(:weight) }
  it { should validate_presence_of(:collector_value) }
end
