require 'rails_helper'

RSpec.describe Capacity, :type => :service do
  let!(:country_a) { Fabricate(:country, name: 'A', code: 'a') }
  let!(:country_b) { Fabricate(:country, name: 'B', code: 'b') }
  let!(:country_c) { Fabricate(:country, name: 'C', code: 'c') }
  let!(:country_d) { Fabricate(:country, name: 'D', code: 'd') }
  let!(:country_v) { Fabricate(:country, name: 'V', code: 'v') }

  let!(:currency_a) do
    Fabricate(:currency,
              name: 'A',
              code: 'A',
              country: country_a,
              weight: 5,
              collector_value: 25)
  end

  let!(:currency_b) do
    Fabricate(:currency,
              name: 'B',
              code: 'B',
              country: country_b,
              weight: 4,
              collector_value: 50)
  end

  let!(:currency_c) do
    Fabricate(:currency,
              name: 'C',
              code: 'C',
              country: country_c,
              weight: 3,
              collector_value: 10)
  end

  let!(:currency_d) do
    Fabricate(:currency,
              name: 'D',
              code: 'D',
              country: country_d,
              weight: 10,
              collector_value: 110)
  end

  let!(:currency_v) do
    Fabricate(:currency,
              name: 'V',
              code: 'V',
              country: country_d,
              weight: 10,
              collector_value: 110)
  end

  let(:user) { Fabricate(:user) }
  let!(:visit) { Fabricate(:visit, user: user, country: country_v) }


  shared_examples "unvisited countries" do
    specify { expect(subject.run.map(&:id)).to eq expected_countries_ids }
  end

  describe '#run' do
    subject { Capacity.new(amount, user) }

    # Capacity 16 returns Countries D & B (1 from D and 1 from B worth 160 and weight of 14)
    # But, B (4B = 200) is bigger value
    context '16' do
      let(:amount) { 16 }
      let(:expected_countries_ids) { %w(B B B B) }
      it_behaves_like 'unvisited countries'
    end

    context '10' do
      let(:amount) { 10 }
      let(:expected_countries_ids) { %w(D) }
      it_behaves_like 'unvisited countries'
    end

    context '12' do
      let(:amount) { 12 }
      let(:expected_countries_ids) { %w(B B B) }
      it_behaves_like 'unvisited countries'
    end

    # Capacity 27 return Countries D & B & C (2D, 1B, 1C = 280)
    # But, B & C (6B, 1C = 360)
    # so, the task is a bit wrong
    context '27' do
      let(:amount) { 27 }
      let(:expected_countries_ids) { %w(B B B B B B C) }
      it_behaves_like 'unvisited countries'
    end

    context '9.5' do
      let(:amount) { 9.5 }
      let(:expected_countries_ids) { %w(A B) }
      it_behaves_like 'unvisited countries'
    end
  end

  context 'internals' do
    subject { Capacity.new(10, user) }
    let(:unvisited) { subject.send(:unvisited) }
    let(:currencies) { subject.send(:currencies) }

    it '#unvisited' do
      expect(unvisited.size).to eq 4
    end

    it '#currencies' do
      expect(unvisited.size).to eq 4
    end
  end

end
