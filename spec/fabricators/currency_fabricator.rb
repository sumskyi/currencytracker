Fabricator(:currency) do
  name 'NameOne'
  code 'CodeOne'
  country
  weight 0.1
  collector_value 2.5
end

Fabricator(:usd, from: :currency) do
  name 'US dollar'
  code 'usd'
  weight 3
  collector_value 3
end

Fabricator(:uah, from: :currency) do
  name 'Ukrainian hryvnya'
  code 'uah'
  weight 4
  collector_value 4
end

Fabricator(:cad, from: :currency) do
  name 'Canadian Dollar'
  code 'cad'
  weight 5
  collector_value 5
end
