Fabricator(:country) do
  name 'One'
  code 'One'
end

Fabricator(:usa, from: :country) do
  name 'United States'
  code 'us'
end

Fabricator(:canada, from: :country) do
  name 'Canada'
  code 'ca'
end

Fabricator(:ukraine, from: :country) do
  name 'Ukraine'
  code 'ua'
end
