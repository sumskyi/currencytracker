RSpec.shared_examples 'not responds to action' do |action|
  it "not responds to" do
    expect{subject}.to raise_error(ActionController::UrlGenerationError)
  end
end

RSpec.shared_context 'logged in user' do
  let(:user) { Fabricate(:user) }
  before { login_user(user) }

  before do
    allow(controller).to receive(:restrict_access).and_return(true)
    allow(controller).to receive(:current_user).and_return(user)
  end
end

RSpec.shared_context 'login via authentication token' do
  let(:user) { Fabricate(:user) }
  before { user.set_authentication_token }
  let(:authentication_token) { user.authentication_token }

  let(:authorization) do
    { authorization: "Token token=#{authentication_token}" }
  end
end
