module Utils
  def parsed_response
    JSON.parse(response.body)
  end
end


RSpec.configure do |config|
  config.include(Utils, :type => :request)
end
