require 'rails_helper'

RSpec.describe Api::CountriesController, type: :controller do
  describe 'disallowed actions' do
    describe '#new' do
      subject { get :new }
      it_behaves_like 'not responds to action'
    end

    describe '#destroy' do
      subject { delete :destroy }
      it_behaves_like 'not responds to action'
    end

    describe '#create' do
      subject { post :create }
      it_behaves_like 'not responds to action'
    end

    describe '#update' do
      subject { put :update }
      it_behaves_like 'not responds to action'
    end
  end

  context 'allowed actions' do
    include_context 'logged in user'

    let!(:country) { Fabricate(:country) }
    let(:country_attributes) { country.attributes }

    describe '#index' do
      it 'success' do
        get :index, format: :json
        expect(response).to be_success
        expect(controller.countries).to be
      end
    end

    describe '#show' do
      it 'shows country' do
        get :show, :id => country.to_param, format: :json
        expect(response).to be_success
      end
    end
  end # allowed actions
end
