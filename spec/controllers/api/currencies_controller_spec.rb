require 'rails_helper'

RSpec.describe Api::CurrenciesController, type: :controller do
  describe 'disallowed actions' do
    describe '#new' do
      subject { get :new }
      it_behaves_like 'not responds to action'
    end

    describe '#destroy' do
      subject { delete :destroy }
      it_behaves_like 'not responds to action'
    end

    describe '#create' do
      subject { post :create }
      it_behaves_like 'not responds to action'
    end

    describe '#edit' do
      subject { get :edit }
      it_behaves_like 'not responds to action'
    end

    describe '#edit' do
      subject { put :update }
      it_behaves_like 'not responds to action'
    end
  end # disallowed

  context 'allowed actions' do
    include_context 'logged in user'

    let(:currency) { Fabricate(:currency) }

    describe '#index' do
      it 'success' do
        get :index, format: :json
        expect(response).to be_success
        expect(controller.currencies).to be
      end
    end

    describe '#show' do
      it 'shows currency' do
        get :show, :id => currency.to_param, format: :json
        expect(response).to be_success
      end
    end
  end
end
