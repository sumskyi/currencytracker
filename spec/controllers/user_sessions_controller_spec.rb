require 'rails_helper'

RSpec.describe UserSessionsController, type: :controller do

  describe "#new" do
    it "returns http success" do
      get :new
      expect(response).to have_http_status(:success)
    end
  end

  describe "#create" do
    context "login failed" do
      it "returns http success" do
        post :create
        expect(response).to have_http_status(:success)
      end
    end

    context "login successful" do
      let!(:user) { Fabricate(:user) }
      it "returns http redirect when login successful" do
        post :create, email: user.email, password: '666666'
        expect(response).to have_http_status(:redirect)
      end
    end
  end

  describe "#destroy" do
    it "returns http success" do
      delete :destroy
      expect(response).to have_http_status(:redirect)
    end
  end

end
