require 'rails_helper'

RSpec.describe UsersController, type: :controller do
  describe "#create" do
    let(:invalid_params) do
      { password: '234' }
    end
    let(:valid_params) do
      Fabricate.attributes_for(:user)
    end

    context "with valid params" do
      it "creates a new User" do
        expect {
          post :create, user: valid_params
        }.to change(User, :count).by(1)
      end

      it "redirects to the created user" do
        post :create, user: valid_params
        expect(response).to redirect_to root_path
      end
    end

    context "with invalid params" do
      it "assigns a newly created but unsaved user as user" do
        post :create, user: invalid_params
        expect(controller.user).to be_a_new(User)
      end

      it "re-renders the 'new' template" do
        post :create, user: invalid_params
        expect(response).to render_template("new")
      end
    end
  end
end
