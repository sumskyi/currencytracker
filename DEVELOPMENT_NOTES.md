Development notes
=================

Setup
-----

* When I was trying to run `bundle exec db:seed` I've got an PK violation error,
and I realized that `countries`.`code` and `currencies`.`code`
had an INTEGER type. It happens with Rails 4.x only, so I've added a new
migration which fixes PK type to string, and added uniq index for it
errors on `db:schema` related rake tasks.
* Removed a lot of junk in vendor/ dir
* Tests rewritten to RSpec, tests/ dir removed completely
* Introduced decent_exposure
* upgraded rails version to 4.2.1, added responders gem
* removed create/update actions from countries controller
* removed UI (excepting creating of users)
* authorization by token
* reqres-rspec self-documenting
* countries & currencies basic API
* added lib for counting weight variants
* added capacity service
* removed managing countries cucumber feature & related paths since it is not relevant (UI is gone)
* added tolerance to fixed weights variants
* added unvisited api endpoint

The last feature mistake
------------------------
