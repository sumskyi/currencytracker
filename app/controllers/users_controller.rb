class UsersController < ApplicationController
  skip_before_action :require_login
  expose(:user, attributes: :user_params)

  def create
    if user.save
      auto_login(user)
      redirect_to root_path,
        notice: t(:user_created, user_params)
    else
      render :new
    end
  end

  private

  def user_params
    params.require(:user).permit(:email, :password, :password_confirmation)
  end
end
