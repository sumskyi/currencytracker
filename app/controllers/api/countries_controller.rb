module Api
  class CountriesController < BaseController
    respond_to :json

    expose(:countries) { Country.all }
    expose(:visited_countries) { Country.visited_by_user(current_user) }
    expose(:country)
    expose(:visits) { Visit.by_user_and_date(current_user, params) }

    expose(:unvisited_countries) { Capacity.new(params[:capacity].to_f, current_user).run }
    expose(:unvisited_grouped) do
      unvisited_countries.each_with_object(Hash.new(0)) do |name, hsh|
        hsh[name] += 1
      end.invert
    end

    def index
    end

    def show
    end

    def visits_over_time
    end

    def visited
    end

    def unvisited
    end

    def visit
      current_user.visits.create!(country: country)
      head :ok
    end
  end
end
