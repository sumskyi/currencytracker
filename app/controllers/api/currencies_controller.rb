module Api
  class CurrenciesController < BaseController
    respond_to :json

    expose(:currencies)
    expose(:currency)
    expose(:visits) { Visit.by_user_and_date(current_user, params) }
    expose(:collected_currencies) do
      visits.map(&:country).map(&:currency)
    end

    def index
    end

    def show
    end

    def collected
    end

    def collected_over_time
    end
  end
end
