# API namespace
module Api
  # API base controller
  class BaseController < ApplicationController
    skip_before_action :require_login
    skip_before_action :verify_authenticity_token
    before_filter :restrict_access, except: :token

    rescue_from Exception, with: :render_500
    rescue_from ActionController::ParameterMissing, with: :render_400

    def token
      if login(params[:email], params[:password])
        current_user.set_authentication_token
        render json: current_user
      else
        render json: { error: 'No such user' }
      end
    end

    private

    def render_400(exception)
      render json: { error: exception.message }, status: 400
    end

    def render_500(exception)
      if Rails.env.test?
        puts exception.message
        puts exception.backtrace
      end
      render json: { error: exception.message }, status: 500
    end

    def restrict_access_method
      if Rails.configuration.allow_url_token && params[:token]
        :authenticate_with_url_token
      else
        :authenticate_with_http_token
      end
    end

    def restrict_access
      send(restrict_access_method, &restrict_access_code) ||
        (render json: { message: 'Invalid API Token' }, status: 401)
    end

    def restrict_access_code
      lambda do |token, _options = {}|
        @current_user = User.find_by(authentication_token: token)
      end
    end

    def authenticate_with_url_token
      yield params[:token]
    end
  end
end
