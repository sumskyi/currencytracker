class ApplicationController < ActionController::Base
  protect_from_forgery

  before_filter :require_login, except: :not_authenticated

  decent_configuration do
    strategy DecentExposure::StrongParametersStrategy
  end

  def not_authenticated
    redirect_to new_user_session_path, alert: 'Please login first.'
  end

  def welcome
  end
end
