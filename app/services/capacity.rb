require 'changer'

class Capacity
  BadMaxweight = Class.new(StandardError)
  TOLERANCE = 1

  def initialize(max_weight, user)
    fail BadMaxweight, max_weight unless max_weight > 0

    @max_weight, @user = max_weight, user
  end

  def run
    return [] if variants.empty?

    max_variant_idx = variants.map(&total_value).each_with_index.max[1]
    variants[max_variant_idx].map(&currency_with_weight)
  end

  private

  def variants
    @_variants ||= charger.run
  end

  def total_value
    -> (el) do
      el.inject(0.0) do |memo, weight|
        memo += currency_with_weight[weight].collector_value.to_f
        memo
      end
    end
  end

  def currency_with_weight
    -> (weight) { Currency.with_weight(weight) }
  end

  def charger
    @_charger ||= Changer.new(@max_weight, weights, TOLERANCE)
  end

  def unvisited
    @_unvisited ||= Country.all - Country.visited_by_user(@user)
  end

  def currencies
    @_currencies ||= unvisited.map(&:currency)
  end

  def weights
    @_weights ||= currencies.map {|c| c.weight.to_i }
  end
end
