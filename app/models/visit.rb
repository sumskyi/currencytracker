class Visit < ActiveRecord::Base
  # remove default ordering error
  # SELECT  "visits".* FROM "visits"  ORDER BY "visits"."" DESC LIMIT 1
  default_scope { order(:user_id) }

  belongs_to :user
  belongs_to :country, foreign_key: :code

  scope :by_user_and_date, -> (user, opts = {}) do
    _scope = where(user_id: user.id).order(:created_at)
    if opts[:start_date] && opts[:end_date]
      _scope = _scope.where('created_at BETWEEN ? AND ?',
                            opts[:start_date],
                            opts[:end_date])
    end
  end
end
