class Currency < ActiveRecord::Base
  self.primary_key = :code

  validates :name, :code, :weight, :collector_value, presence: true
  validates :code, uniqueness: true

  belongs_to :country

  scope :with_weight, ->(weight) { where(weight: weight).take }
end
