class User < ActiveRecord::Base
  authenticates_with_sorcery!

  validates :email, presence: true, uniqueness: true

  with_options if: :password_required? do |user|
    user.validates :password, length: { minimum: 3 }
    user.validates :password, presence: true
    user.validates :password_confirmation, presence: true
    user.validates :password, confirmation: true
  end

  has_many :visits
  has_many :countries, through: :visits
  has_many :currencies, through: :countries

  def set_authentication_token
    loop do
      self.authentication_token = SecureRandom.hex.to_s
      break unless self.class.exists?(authentication_token: authentication_token)
    end
    save!
  end

  def as_json(options = {})
    super options.merge(only: :authentication_token)
  end

  private

  def password_required?
    crypted_password.blank? || !password.blank?
  end
end
