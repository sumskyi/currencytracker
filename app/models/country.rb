class Country < ActiveRecord::Base
  self.primary_key = :code

  validates :name, :code, presence: true
  validates :code, uniqueness: true

  # old assoc was has_many :/
  # I believe there are no countries having multiple currencies
  has_one :currency

  has_many :visits, foreign_key: :code
  has_many :users, through: :visits

  scope :visited_by_user, ->(user) {
    eager_load(:visits).where('visits.user_id = ?', user.id)
  }
end
