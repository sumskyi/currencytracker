my_formats = {
  date_db: '%Y-%m-%d'
}

Time::DATE_FORMATS.merge!(my_formats)
Date::DATE_FORMATS.merge!(my_formats)