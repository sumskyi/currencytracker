class AddCreatedAtToVisit < ActiveRecord::Migration
  def change
    add_column :visits, :id, :primary_key
    add_column :visits, :created_at, :datetime
  end
end
