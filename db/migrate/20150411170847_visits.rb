class Visits < ActiveRecord::Migration
  def change
    create_table :visits, id: false do |t|
      t.integer :user_id, null: false
      t.string  :code,    null: false
    end
    add_index :visits, [:user_id, :code], unique: true
  end
end
