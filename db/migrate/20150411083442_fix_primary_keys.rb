# Rails 4 fix for INTEGER PK only
#
# We are about use uniq nut null field instead of real PK
# A UNIQUE constraint is similar to a PRIMARY KEY constraint,
# except that a single table may have any number of UNIQUE constraints
class FixPrimaryKeys < ActiveRecord::Migration
  def change
    remove_column :countries, :code
    add_column :countries, :code, :string, null: false, default: ''
    add_index :countries, :code, unique: true

    remove_column :currencies, :code
    add_column :currencies, :code, :string, null: false, default: ''
    add_index :currencies, :code, unique: true
  end
end
