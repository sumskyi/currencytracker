class RemoveVisitedFromCountries < ActiveRecord::Migration
  def change
    remove_column :countries, :visited, :string
  end
end
